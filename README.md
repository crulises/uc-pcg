Planeamiento y Control de Gestión
=================================

Equipo de cátedra
-----------------
| Cargo | Nombre |
| --------|--------- |
| Adjunto | Mgter. Claudia Martinez |
| JTP | Lic. Flabio Ledesma |

Administrador del Repo
----------------------
| Alumno | Legajo |
|---------|---------|
| Ramirez Ulises | LS00704 |

